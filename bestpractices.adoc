
== Best practices

=== Erzeugung und Platzierung

Wie bei den Immobilien -- Lage ist alles!

==== Nutzlast kurz halten

Je kürzer die Nutzlast, umso kleiner kann die _Version_ des QR-Codes werden und umso einfacher ist er zu scannen.

==== Testen, testen, testen



==== Ist die Information statisch?

Beispiel Kontaktdaten: abwägen zwischen statischem Inhalt und Link auf vCard

==== Wird ein Tracking benötigt?

Dann bleibt eigentlich nur eine URL und die entsprechenden Tracking-Mechanismen.

==== QR-Codes sind für offline!

Keine QR-Codes in Online-Publikationen, besser kurze Links

==== Immer auch einen Kurzlink angeben

Auch wenn ein QR-Code offline genutzt wird, ist ein guter Kurzlink oft einfacher,
vor allem für Benutzer, deren Smartphone noch keinen in der Kamera integrierten QR-Leser besitzt.

==== Klare Anweisung zur Nutzung

Ausserhalb der Schule (bzw. der technik-affinen Blase) sind QR-Coces kaum verbreitet.

==== Positinierung gut planen

	Augenhöhe
	Größe : Abstand = 1 : 10 => 50cm Abstand 5cm QR-Code

==== Accessibility

Aber richtig geplant: Link zu einer Audio-Version für Sehbehinderte ist Quark :-/
Besser: weitere Details hinter den Code, Multimedia-Assets, andere Sprachen.

=== Nutzung

==== Erkannte Codes nicht direkt ausführen

Lese-Apps so einstellen, dass die Nutzlast nicht direkt aktiviert wird

==== Lösungen für Aufgaben

 (sind ja meist weniger als 1-2 Kilobyte Text), eine A4-Seite voll ca. 3,5 KByte

==== Binnendifferenzierung

Verschiedene Anforderungsstufen, Zusatzaufgaben oder zusätzliche Erklärungen

==== Schnitzeljagd mit Abschluss-Benachrichtigung

Hier wird's allerdings schnell Technik-Spielerei.

==== Schnelle Dokumentationen

Abwägung direkt vs link

==== Verbindung von analogem und digitalem Material

Beispiele für QR-Codes in der Kulturvermittlung bietet http://culture-to-go.com/mediathek/qr-codes-in-der-kulturvermittlung/

=== How not to do it

Beispiele aus https://wtfqrcodes.com





